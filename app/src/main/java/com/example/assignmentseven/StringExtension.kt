package com.example.assignmentseven

import android.util.Patterns
import java.util.regex.Pattern

fun String.validateEmail():Boolean
{
    if(Patterns.EMAIL_ADDRESS.matcher(this).matches())
        return true

    return false
}