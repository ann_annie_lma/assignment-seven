package com.example.assignmentseven

import android.widget.EditText
import androidx.core.content.res.ResourcesCompat

fun EditText.setBackground()
{
    this.setBackgroundResource(R.drawable.edit_fields_error)
}

