package com.example.assignmentseven

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.example.assignmentseven.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSignIn.setOnClickListener {
           if(validateFields())
           {

           }
        }

        binding.imgPasswordIcon.setOnClickListener {
            makePasswordVisible(it)
        }

    }

    private fun validateFields():Boolean
    {
        if(!binding.etEmail.text.toString().validateEmail())
        {
            binding.etEmail.setBackground()
            binding.txtErrorMessage.setText(R.string.enter_valid_email)
            binding.txtErrorMessage.setTextColor(resources.getColor(R.color.main_text_second))
            return false
        }
        if(binding.etPassword.text.toString().length < 8)
        {
            binding.etPassword.setBackground()
            binding.txtPassword.setText(R.string.password_error)
            binding.txtPassword.setTextColor(resources.getColor(R.color.main_text_second))

            return false
        }

        return true
    }

    fun makePasswordVisible(res: View)
    {

        if(res.id == R.id.img_password_icon)
        {
            if(binding.etPassword.transformationMethod.
                equals(PasswordTransformationMethod.getInstance()))
            {
                binding.imgPasswordIcon.setImageResource(R.drawable.component_1___12)
                binding.etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else
            {
                binding.imgPasswordIcon.setImageResource(R.drawable.shown)
                binding.etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            }

        }
    }


}